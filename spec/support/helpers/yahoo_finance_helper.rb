module YahooFinanceHelper
  def yahoo_not_found_body
    {
      "chart" => {
        "result" => nil, "error" => {"code" => "Not Found", "description" => "No data found, symbol may be delisted"}
      }
    }
  end

  def yahoo_response_body
    {
      "chart" => {
        "result" => [
          {
            "meta" => {
              "currency" => "BRL",
              "symbol" => "PETR4.SA",
              "exchangeName" => "SAO",
              "instrumentType" => "EQUITY",
              "firstTradeDate" => 946900800,
              "regularMarketTime" => 1670015268,
              "gmtoffset" => -7200,
              "timezone" => "BRST",
              "exchangeTimezoneName" => "America/Sao_Paulo",
              "regularMarketPrice" => 25.91,
              "chartPreviousClose" => 23.86,
              "priceHint" => 2,
              "currentTradingPeriod" => {
                "pre" => {"timezone" => "BRST", "start" => 1669981500, "end" => 1669982400, "gmtoffset" => -7200},
                "regular" => {"timezone" => "BRST", "start" => 1669982400, "end" => 1670007600, "gmtoffset" => -7200},
                "post" => {"timezone" => "BRST", "start" => 1670007600, "end" => 1670011200, "gmtoffset" => -7200}
              },
              "dataGranularity" => "1d",
              "range" => "5d",
              "validRanges" => ["1d", "5d", "1mo", "3mo", "6mo", "1y", "2y", "5y", "10y", "ytd", "max"]
            },
            "timestamp" => [1669636800, 1669723200, 1669809600, 1669896000, 1669982400],
            "indicators" => {
              "quote" => [
                {
                  "open" => [23.649999618530273, 24.600000381469727, 25.600000381469727, 26.579999923706055, 25.690000534057617],
                  "close" => [24.360000610351562, 25.3799991607666, 26.65999984741211, 25.59000015258789, 25.90999984741211],
                  "volume" => [56067400, 96151500, 120774700, 71259400, 66301000],
                  "high" => [24.549999237060547, 25.8799991607666, 26.65999984741211, 26.790000915527344, 26.450000762939453],
                  "low" => [23.540000915527344, 24.479999542236328, 25.5, 25.559999465942383, 25.309999465942383]
                }
              ],
              "adjclose" => [{"adjclose" => [24.360000610351562, 25.3799991607666, 26.65999984741211, 25.59000015258789, 25.90999984741211]}]
            }
          }
        ],
        "error" => nil
      }
    }
  end
end
