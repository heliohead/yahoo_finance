require "rails_helper"

RSpec.describe Stock, type: :model do
  context "with validations" do
    it "validate presence of fields" do
      stock = described_class.new

      expect(stock.valid?).to be false
      expect(stock.errors.messages.keys).to eq(%i[symbol date open_price])
    end
  end
end
