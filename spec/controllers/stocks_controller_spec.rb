require "rails_helper"

RSpec.describe StocksController, type: :controller do
  context "GET /stocks/:symbol" do
    it "return 200" do
      mock_response = instance_double(HTTParty::Response, body: yahoo_response_body.to_json)
      allow(HTTParty).to receive(:get).and_return(mock_response)
      get :index, params: {symbol: "PETR4.SA"}

      stock_attributes = %w[id symbol date open_price created_at updated_at d1_percentage first_day_percentage]

      expect(response.code).to eq("200")
      expect(response_body_json["data"].size).to eq(5)
      expect(response_body_json["data"].first.keys).to match_array(stock_attributes)
    end

    it "return 400 if symbol is not present" do
      get :index

      expect(response.code).to eq("400")
      expect(response_body_json).to eq({"detail" => {"symbol" => ["is missing"]}})
    end

    it "return 404 if symbol is not recognized" do
      mock_response = instance_double(HTTParty::Response, body: yahoo_not_found_body.to_json)
      allow(HTTParty).to receive(:get).and_return(mock_response)
      get :index, params: {symbol: "FOOBAR"}

      expect(response.code).to eq("404")
      expect(response_body_json).to eq({"detail" => {"code" => "Not Found", "description" => "No data found, symbol may be delisted"}})
    end
  end

  def response_body_json
    JSON.parse(response.body)
  end
end
