require "rails_helper"

RSpec.describe FetchStocksService, type: :service do
  context "with params" do
    it "respond to service contract" do
      Stock.create!(symbol: "PETR4.SA", date: Date.today, open_price: 42)

      subject = described_class.call(params: "PETR4.SA")

      expect(subject[:status]).to eq(:ok)
      expect(subject[:payload]).not_to be nil
      expect(subject[:error]).to be nil
    end

    it "return stocks calling yahoo if have no stock in db" do
      mock_response = instance_double(HTTParty::Response, body: yahoo_response_body.to_json)
      allow(HTTParty).to receive(:get).and_return(mock_response)

      subject = described_class.call(params: "PETR4.SA")

      expect(subject[:status]).to eq(:ok)
      expect(subject[:payload].size).to eq(5)
      expect(subject[:error]).to be nil
    end

    it "return stocks calling yahoo if have no stock in db" do
      mock_response = instance_double(HTTParty::Response, body: yahoo_not_found_body.to_json)
      allow(HTTParty).to receive(:get).and_return(mock_response)

      subject = described_class.call(params: "unknown")

      expect(subject[:status]).to eq(:error)
      expect(subject[:payload]).to be nil
      expect(subject[:detail]).not_to be nil
    end
  end
end
