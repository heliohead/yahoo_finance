require "rails_helper"

RSpec.describe YahooFinanceService, type: :service do
  context "with symbol" do
    it "call yahoo api and parse results" do
      mock_response = instance_double(HTTParty::Response, body: yahoo_response_body.to_json)
      allow(HTTParty).to receive(:get).and_return(mock_response)

      response = described_class.call(symbol: "PETR4.SA", range: "5d", interval: "1d")

      expected_response = [
        {
          symbol: "PETR4.SA",
          date: Date.new(2022, 11, 28),
          open_price: 23.649999618530273
        },
        {
          symbol: "PETR4.SA",
          date: Date.new(2022, 11, 29),
          open_price: 24.600000381469727
        },
        {
          symbol: "PETR4.SA",
          date: Date.new(2022, 11, 30),
          open_price: 25.600000381469727
        },
        {
          symbol: "PETR4.SA",
          date: Date.new(2022, 12, 1),
          open_price: 26.579999923706055
        },
        {
          symbol: "PETR4.SA",
          date: Date.new(2022, 12, 2),
          open_price: 25.690000534057617
        }
      ]

      expect(response[:status]).to eq(:ok)
      expect(response[:payload]).to eq(expected_response)
      expect(response[:detail]).to be nil
    end

    it "respond with yahoo response if symbol not exists" do
      mock_response = instance_double(HTTParty::Response, body: yahoo_not_found_body.to_json)
      allow(HTTParty).to receive(:get).and_return(mock_response)

      response = described_class.call(symbol: "PETR4.SA", range: "5d", interval: "1d")

      expect(response[:status]).to eq(:error)
      expect(response[:detail]).to eq(yahoo_not_found_body.dig("chart", "error"))
      expect(response[:payload]).to be nil
    end
  end
end
