require "rails_helper"

RSpec.describe StocksPercentageService, type: :service do
  context "with params" do
    it "respond to service contract" do
      subject = described_class.call(stocks: [])

      expect(subject[:status]).to eq(:ok)
      expect(subject[:payload]).not_to be nil
      expect(subject[:error]).to be nil
    end

    it "stocks prices with percentage" do
      stocks = [
        Stock.create!(symbol: "PETR4.SA", date: Date.new(2021, 1, 1), open_price: 1.0),
        Stock.create!(symbol: "PETR4.SA", date: Date.new(2021, 1, 2), open_price: 1.10),
        Stock.create!(symbol: "PETR4.SA", date: Date.new(2021, 1, 3), open_price: 1.05),
        Stock.create!(symbol: "PETR4.SA", date: Date.new(2021, 1, 3), open_price: 1.90)
      ]

      response = described_class.call(stocks: stocks)
      subject = response[:payload]

      expect(subject.first.d1_percentage).to be nil
      expect(subject.first.first_day_percentage).to be nil
      expect(subject[1].d1_percentage.to_f.round(3)).to eq(10.0)
      expect(subject[1].first_day_percentage.to_f.round(3)).to eq(10.0)
      expect(subject[2].d1_percentage.to_f.round(3)).to eq(-4.545)
      expect(subject[2].first_day_percentage.to_f.round(3)).to eq(5.0)
      expect(subject.last.d1_percentage.to_f.round(3)).to eq(80.952)
      expect(subject.last.first_day_percentage.to_f.round(3)).to eq(90.0)
    end
  end
end
