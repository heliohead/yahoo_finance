class CreateStocks < ActiveRecord::Migration[7.0]
  def change
    create_table :stocks do |t|
      t.string :symbol, index: true
      t.date :date
      t.decimal :open_price, precision: 15, scale: 10

      t.timestamps
    end
  end
end
