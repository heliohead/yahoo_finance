class ApplicationController < ActionController::API
  def render_params_error
    render json: {detail: safe_params.errors.to_h}, status: :bad_request
  end

  def render_not_found
    render json: {detail: "Not found"}, status: :not_found
  end
end
