class StocksController < ApplicationController
  schema(:index) do
    required(:symbol).value(:string)
  end

  def index
    render_params_error and return if safe_params.failure?

    case FetchStocksService.call(params: safe_params[:symbol])
    in {status: :ok, payload: payload}
      render json: {data: payload}, status: :ok
    in {status: :error, detail: detail}
      render json: {detail: detail}, status: :not_found
    end
  end
end
