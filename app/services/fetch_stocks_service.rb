class FetchStocksService
  include ApplicationService

  def initialize(params:)
    @symbol = params
  end

  def call
    return call_yahoo_api if stocks.empty?

    StocksPercentageService.call(stocks: stocks)
  end

  private

  attr_reader :symbol

  def call_yahoo_api
    case YahooFinanceService.call(symbol: symbol)
    in {status: :ok, payload: payload}
      create_stocks(payload)
    in {status: :error, detail: detail}
      return {status: :error, detail: detail}
    end
  end

  def stocks
    @stock ||= Stock.where(symbol: symbol).order(date: :desc).limit(30).reverse
  end

  def create_stocks(payload)
    stocks = Stock.create(payload)

    StocksPercentageService.call(stocks: stocks)
  end
end
