class YahooFinanceService
  include ApplicationService

  def initialize(symbol:, range: "1mo", interval: "1d")
    @symbol = symbol
    @range = range
    @interval = interval
  end

  def call
    parse_response
  end

  private

  attr_reader :symbol, :range, :interval

  def parse_response
    if response.dig("chart", "result").present?
      timestamps = response["chart"]["result"][0]["timestamp"]
      open_values = response["chart"]["result"][0]["indicators"]["quote"][0]["open"]

      {status: :ok, payload: parse(timestamps, open_values)}
    else
      {status: :error, detail: response.dig("chart", "error")}
    end
  end

  def parse(timestamps, open_values)
    timestamps.zip(open_values).map do |aggr|
      {
        symbol: symbol,
        date: epoch_to_date(aggr.first),
        open_price: aggr.last
      }
    end
  end

  def response
    @response ||= JSON.parse(yahoo_api_call.body)
  end

  def yahoo_api_call
    HTTParty.get("https://query2.finance.yahoo.com/v8/finance/chart/#{symbol}", {
      headers: {"User-Agent" => "Httparty"},
      query: {"interval" => interval, "range" => range}
    })
  rescue => e
    Rails.logger.error(e)

    {"chart" => {"error" => "Error on communication"}}
  end

  def epoch_to_date(epoch)
    Time.at(epoch).to_date
  end
end
