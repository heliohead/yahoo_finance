class StocksPercentageService
  include ApplicationService

  def initialize(stocks:)
    @stocks = stocks
  end

  def call
    {status: :ok, payload: stocks_with_percentage}
  end

  private

  attr_reader :stocks

  def stocks_with_percentage
    stocks.map.with_index do |stock, index|
      stock.d1_percentage = d1_percentage(stock, index)
      stock.first_day_percentage = first_day_percentage(stock, index)
      stock
    end
  end

  def d1_percentage(stock, index)
    return if index == 0

    last_price = stocks[index - 1].open_price
    (stock.open_price - last_price) / last_price * 100
  end

  def first_day_percentage(stock, index)
    return if index == 0

    (stock.open_price - stocks.first.open_price) / stocks.first.open_price * 100
  end
end
