module ApplicationService
  extend ActiveSupport::Concern

  module ClassMethods
    def call(**args, &block)
      new(**args).call(&block)
    end
  end

  def call
    raise NotImplementedError
  end
end
