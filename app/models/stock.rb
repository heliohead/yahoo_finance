class Stock < ApplicationRecord
  attribute :d1_percentage
  attribute :first_day_percentage

  validates_presence_of :symbol, :date, :open_price
end
