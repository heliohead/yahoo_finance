# README

This is a dummy project so some decisions made was very experimental, used of
pattern matching is one of those


## Up and Running

- Build

```sh
$ git clone
$ bin/rails db:create db:migrate
```

- Testing

```sh
$ bundle exec rspec
```

- Using

```sh
$ bundle exec rails s
```

- Request examples

```sh
$ curl -I -X GET "localhost:3000/stocks/any"
HTTP/1.1 404 Not Found
Content-Type: application/json; charset=UTF-8
X-Request-Id: 1afdb297-4790-4c08-bba6-39ab481089ed
X-Runtime: 0.004566
Server-Timing:
Content-Length: 6325

```

```sh
$ curl -X GET "localhost:3000/stocks"
{"detail":{"symbol":["is missing"]}}

```

```sh
❯ curl -X GET "localhost:3000/stocks?symbol=PETR4.SA"
{"data":[{"id":1,"symbol":"PETR4.SA","date":"2022-11-03","open_price":"29.7999992371","created_at":"2022-12-04T21:41:53.312Z","updated_at":"2022-12-04T21:41:53.312Z","d1_percentage":null,"first_day_percentage":null},{"id":2,"symbol":"PETR4.SA","date":"2022-11-04","open_price":"30.7999992371","created_at":"2022-12-04T21:41:53.315Z","updated_at":"2022-12-04T21:41:53.315Z","d1_percentage":"3.355704783894871799778446176207268","first_day_percentage":"3.355704783894871799778446176207268"},{"id":3,"symbol":"PETR4.SA","date":"2022-11-07","open_price":"27.9599990845","created_at":"2022-12-04T21:41:53.317Z","updated_at":"2022-12-04T21:41:53.317Z","d1_percentage":"-9.2207799446276954466385992285904984","first_day_percentage":"-6.1744973144471141339497776104122597"},{"id":4,"symbol":"PETR4.SA","date":"2022-11-08","open_price":"27.0599994659","created_at":"2022-12-04T21:41:53.318Z","updated_at":"2022-12-04T21:41:53.318Z","d1_percentage":"-3.2188828614766544950599853443282039603887956","first_day_percentage":"-9.1946303400866941762462747334994293"},{"id":5,"symbol":"PETR4.SA","date":"2022-11-09","open_price":"27.1499996185","created_at":"2022-12-04T21:41:53.319Z","updated_at":"2022-12-04T21:41:53.319Z","d1_percentage":"0.3325948055298922998342748463224758100825695","first_day_percentage":"-8.8926163974556056919087779314498887"},{"id":6,"symbol":"PETR4.SA","date":"2022-11-10","open_price":"26.0799999237","created_at":"2022-12-04T21:41:53.320Z","updated_at":"2022-12-04T21:41:53.320Z","d1_percentage":"-3.9410670712160989933311884329962574","first_day_percentage":"-12.4832194920620184729568420476098926"},{"id":7,"symbol":"PETR4.SA","date":"2022-11-11","open_price":"25.7999992371","created_at":"2022-12-04T21:41:53.321Z","updated_at":"2022-12-04T21:41:53.321Z","d1_percentage":"-1.0736222677115559442634861406174843761163366","first_day_percentage":"-13.4228191355794871991137847048290722"},{"id":8,"symbol":"PETR4.SA","date":"2022-11-14","open_price":"27.2600002289","created_at":"2022-12-04T21:41:53.322Z","updated_at":"2022-12-04T21:41:53.322Z","d1_percentage":"5.6589187402011281743969257460238237","first_day_percentage":"-8.5234868229049697045034022673035433"},{"id":9,"symbol":"PETR4.SA","date":"2022-11-16","open_price":"27.8099994659","created_at":"2022-12-04T21:41:53.323Z","updated_at":"2022-12-04T21:41:53.323Z","d1_percentage":"2.0176054012534894957107943665370201577283965","first_day_percentage":"-6.6778517521655403264124401013439783"},{"id":10,"symbol":"PETR4.SA","date":"2022-11-17","open_price":"27.0","created_at":"2022-12-04T21:41:53.324Z","updated_at":"2022-12-04T21:41:53.324Z","d1_percentage":"-2.9126194946289130557821813410019077392707272","first_day_percentage":"-9.3959708348384614059819532424037627"},{"id":11,"symbol":"PETR4.SA","date":"2022-11-18","open_price":"27.3999996185","created_at":"2022-12-04T21:41:53.325Z","updated_at":"2022-12-04T21:41:53.325Z","d1_percentage":"1.4814800685185185185185185","first_day_percentage":"-8.0536902014818877419641663873980717"},{"id":12,"symbol":"PETR4.SA","date":"2022-11-21","open_price":"27.1200008392","created_at":"2022-12-04T21:41:53.326Z","updated_at":"2022-12-04T21:41:53.326Z","d1_percentage":"-1.0218933693376759270920936564099901430807022","first_day_percentage":"-8.9932834446636221454321253271868595"},{"id":13,"symbol":"PETR4.SA","date":"2022-11-22","open_price":"23.0","created_at":"2022-12-04T21:41:53.327Z","updated_at":"2022-12-04T21:41:53.327Z","d1_percentage":"-15.1917430372820517372014078960390301","first_day_percentage":"-22.8187899704179486050957379472328349"},{"id":14,"symbol":"PETR4.SA","date":"2022-11-23","open_price":"23.0","created_at":"2022-12-04T21:41:53.328Z","updated_at":"2022-12-04T21:41:53.328Z","d1_percentage":"0.0","first_day_percentage":"-22.8187899704179486050957379472328349"},{"id":15,"symbol":"PETR4.SA","date":"2022-11-24","open_price":"23.5200004578","created_at":"2022-12-04T21:41:53.329Z","updated_at":"2022-12-04T21:41:53.329Z","d1_percentage":"2.2608715556521739130434783","first_day_percentage":"-21.073821946550965202138635997032396"},{"id":16,"symbol":"PETR4.SA","date":"2022-11-25","open_price":"24.2600002289","created_at":"2022-12-04T21:41:53.330Z","updated_at":"2022-12-04T21:41:53.330Z","d1_percentage":"3.1462574689474205236339661683830904810468188","first_day_percentage":"-18.5906011745895851038387407959253474"},{"id":17,"symbol":"PETR4.SA","date":"2022-11-28","open_price":"23.6499996185","created_at":"2022-12-04T21:41:53.331Z","updated_at":"2022-12-04T21:41:53.331Z","d1_percentage":"-2.5144295327472003278716341694222151120880033","first_day_percentage":"-20.6375831410876569911333395481753269"},{"id":18,"symbol":"PETR4.SA","date":"2022-11-29","open_price":"24.6000003815","created_at":"2022-12-04T21:41:53.332Z","updated_at":"2022-12-04T21:41:53.332Z","d1_percentage":"4.0169166102517415142088536012971521731443363","first_day_percentage":"-17.4496610359847786695566324498239898"},{"id":19,"symbol":"PETR4.SA","date":"2022-11-30","open_price":"25.6000003815","created_at":"2022-12-04T21:41:53.334Z","updated_at":"2022-12-04T21:41:53.334Z","d1_percentage":"4.0650405873653258504117149621110058","first_day_percentage":"-14.0939562520899068697781862736167217"},{"id":20,"symbol":"PETR4.SA","date":"2022-12-01","open_price":"26.5799999237","created_at":"2022-12-04T21:41:53.335Z","updated_at":"2022-12-04T21:41:53.335Z","d1_percentage":"3.8281231546707428317621722532316908356292948","first_day_percentage":"-10.8053671001145825730676189595062585"},{"id":21,"symbol":"PETR4.SA","date":"2022-12-02","open_price":"25.6900005341","created_at":"2022-12-04T21:41:53.336Z","updated_at":"2022-12-04T21:41:53.336Z","d1_percentage":"-3.3483799554357182317436155410849460415643861","first_day_percentage":"-13.7919423094588183854406894715671811"}]}

```
